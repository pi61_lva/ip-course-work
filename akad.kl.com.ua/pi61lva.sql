-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Июн 09 2020 г., 21:37
-- Версия сервера: 10.3.22-MariaDB-54+deb10u1-log
-- Версия PHP: 7.1.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `pi61lva`
--

-- --------------------------------------------------------

--
-- Структура таблицы `aboutus_clientfeedback`
--

CREATE TABLE `aboutus_clientfeedback` (
  `id` int(11) NOT NULL,
  `client_img` varchar(100) NOT NULL,
  `client_name` varchar(80) NOT NULL,
  `text` varchar(400) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `aboutus_clientfeedback`
--

INSERT INTO `aboutus_clientfeedback` (`id`, `client_img`, `client_name`, `text`) VALUES
(1, 'Layer 13.png', 'The Merchant', 'Quisque iaculis lorem vestibulum eros vehicula, non congue elit dictum. Donec mollis aliquet lorem, eu porttitor sapien semper in. Duis ac erat luctus, gravida lectus sit amet, consectetur orci. Suspendisse libero mauris.');

-- --------------------------------------------------------

--
-- Структура таблицы `aboutus_info`
--

CREATE TABLE `aboutus_info` (
  `id` int(11) NOT NULL,
  `section_title` varchar(100) NOT NULL,
  `small_title` varchar(100) NOT NULL,
  `text` varchar(1000) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `aboutus_info`
--

INSERT INTO `aboutus_info` (`id`, `section_title`, `small_title`, `text`) VALUES
(1, 'ABOUT US', 'WE ARE AWESOME', 'AKAD is a California-based digital agency founded in 2010. Their team works with enterprise clients on their UX design systems, design, development, and branding needs.\nAKAD developed a website focused on employee benefits for a Fortune 500 media group. Isadora began their work by assessing the company\'s existing site and researching how other companies designed sites geared toward employee benefits. They created a new look for the site creating all designs from scratch and integrating with WordPress, which ultimately helped form a new strategic marketing tool.'),
(2, 'WHAT WE DO', 'CREATIVE & DIGITAL', 'Whether it’s a pop-up meal, a visual study of a sector, or a training kit, all Creative Agency projects are influenced by systems and people-oriented thinking, and end in targeted insights, programs or products that move forward your organizational goals.\nAll Creative Agency projects start with a discovery phase in which we understand the core goals of your effort and apply the Creative Agency approach to understanding the larger opportunities you might have. Creative Agency projects can be categorized into three major types that are linked by a continuum of transforming information into action and experience.\n');

-- --------------------------------------------------------

--
-- Структура таблицы `aboutus_ourclient`
--

CREATE TABLE `aboutus_ourclient` (
  `id` int(11) NOT NULL,
  `logo` varchar(100) NOT NULL,
  `webpage` varchar(70) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `aboutus_ourclient`
--

INSERT INTO `aboutus_ourclient` (`id`, `logo`, `webpage`) VALUES
(1, 'img1.jpg', 'https://steak-house.com.ua/'),
(2, 'img2.jpg', 'https://worldarchery.org/archery-equipment'),
(3, 'img3.jpg', 'https://atelierdelarmee.com/'),
(4, 'img4.jpg', 'https://www.behance.net/gallery/13705129/Pacificana-Co'),
(5, 'img5.jpg', 'https://www.theknot.com/'),
(6, 'img6.jpg', 'http://www.themerchantatl.com/');

-- --------------------------------------------------------

--
-- Структура таблицы `aboutus_team`
--

CREATE TABLE `aboutus_team` (
  `id` int(11) NOT NULL,
  `img_src` varchar(100) NOT NULL,
  `name` varchar(80) NOT NULL,
  `position` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `aboutus_team`
--

INSERT INTO `aboutus_team` (`id`, `img_src`, `name`, `position`) VALUES
(1, 'dreamteam1.jpg', 'Jonh Doe', 'CEO / CO-FOUNDER/'),
(2, 'dreamteam2.jpg', 'JENNIFER TOMS', 'PROJECT CHIED'),
(3, 'dreamteam3.jpg', 'JACK DEC', 'GRAPHIC DESIGNER'),
(4, 'dreamteam4.jpg', 'ANNE TURIN', 'ART DIRECTOR');

-- --------------------------------------------------------

--
-- Структура таблицы `blogpost`
--

CREATE TABLE `blogpost` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `publication_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `short_text` varchar(500) NOT NULL,
  `all_text` varchar(10000) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `blogpost`
--

INSERT INTO `blogpost` (`id`, `title`, `category_id`, `publication_date`, `short_text`, `all_text`) VALUES
(7, 'The magic of Virtualization: Proxmox VE introductory course', 29, '2020-06-09 12:38:25', '<p><img alt=\"\" src=\"/public/upload/images/jpbztydtlwpysyurojsagkrga84.png\" style=\"height:760px; width:1520px\" /></p>\r\n\r\n<p>Today, I am going to explain how to quickly deploy several virtual servers with different operating systems on a single physical server without much effort. This will enable any system administrator to manage the whole corporate IT infrastructure in a centralized manner and save a huge amount of resources.</p>\r\n', '<p><img alt=\"\" src=\"/public/upload/images/jpbztydtlwpysyurojsagkrga84.png\" style=\"height:760px; width:1520px\" /></p>\r\n\r\n<p>Today, I am going to explain how to quickly deploy several virtual servers with different operating systems on a single physical server without much effort. This will enable any system administrator to manage the whole corporate IT infrastructure in a centralized manner and save a huge amount of resources.<br />\r\n<br />\r\nVirtualization helps reach maximum abstraction from physical server equipment, protect critical services and easily restore their operation after serious failures.<br />\r\n<br />\r\nMost system administrators are certainly aware of virtual environment techniques, so this article may not be a revelation to them. Nevertheless, some companies still do not use flexible and fast virtual solutions because of a lack of reliable information. We hope that this article&#39;s case study will help you realize that it is much simpler to start using virtualization once, rather than experience the inconveniences and drawbacks of physical infrastructure.<br />\r\n<br />\r\nThe good news is that it is quite easy to try using virtualization. We will demonstrate how to create a server in a virtual network, for example, to migrate a corporate CRM system. Almost any physical server can be transformed into a virtual server, but you need to learn the basic techniques first. These are described below.</p>\r\n\r\n<h2><strong>How it works</strong></h2>\r\n\r\n<p><br />\r\nWhen it comes to virtualization, many beginners face problems understanding the terminology. So, let&#39;s explain some basic concepts:<br />\r\n&nbsp;</p>\r\n\r\n<ul>\r\n	<li><strong>A hypervisor</strong>&nbsp;is a special software for creating and managing virtual machines.</li>\r\n	<li><strong>A virtual machine (&laquo;VM&raquo;)</strong>&nbsp;is a system that operates as a logical server inside a physical server, with its individual set of features, storage media, and operating system.</li>\r\n	<li><strong>A virtualization host</strong>&nbsp;is a physical server with a running hypervisor.</li>\r\n</ul>\r\n\r\n<p><br />\r\nTo make a server a full-featured virtualization host, its processor must either support Intel VT or AMD-V technology. Both technologies perform a fundamental task &mdash; allocating server hardware resources to virtual machines.<br />\r\n<br />\r\nThe key feature is that any virtual machine operations are performed directly at the hardware level. However, they remain isolated from one another; so, it is quite easy to manage them individually. The hypervisor&#39;s role is to control and distribute resources, roles, and priorities between them. In addition, the hypervisor emulates the part of the hardware which is required for the operating system to function correctly.<br />\r\n&nbsp;</p>\r\n'),
(8, 'Samyang AF 85mm F1.4 RF Review', 30, '2020-06-09 14:11:56', '<p><img alt=\"\" src=\"/public/upload/images/samyang_af_85mm_f1_4_rf_review.jpg\" style=\"height:413px; width:550px\" /></p>\r\n\r\n<p>The Samyang AF 85mm f/1.4 RF is a very fast classic portrait prime lens for Canon EOS R full-frame mirrorless cameras. Competitively priced at &pound;599 / $699, is the Samyang 85mm RF a compelling alternative to Canon&#39;s own 85mm lenses? Read our Samyang AF 85mm f/1.4 RF review, complete with full-size sample images, to find out...</p>\r\n', '<h2><img alt=\"\" src=\"/public/upload/images/samyang_af_85mm_f1_4_rf_review.jpg\" style=\"height:413px; width:550px\" /></h2>\r\n\r\n<h2>Introduction</h2>\r\n\r\n<p>The Samyang AF 85mm F1.4 RF (also known as the Rokinon&nbsp;AF 85mm F1.4 RF in the USA) is a very fast short telephoto (portrait) prime lens for full-frame Canon R-series mirrorless cameras.<br />\r\n<br />\r\nThe Samyang AF 85mm F1.4 RF is comprised of 11 elements in 8 groups, including 4 high refractive index elements and 1 extra-low dispersion element. It features a rounded 9 blade diaphragm which creates an attractive blur to out of focus areas of the image.<br />\r\n<br />\r\nIt&#39;s also the latest Samyang lens to feature auto-focusing, incorporating an Ultrasonic Dual Linear motor for quick, quiet autofocus performance.<br />\r\n<br />\r\nThis lens boasts a weather-sealed design and there&#39;s an&nbsp;Ultra Multi-layer Coating to help reduce flare and ghosting and it has a minimum focusing distance of 0.90m (2.95ft) and a maximum reproduction ratio of 0.11x.<br />\r\n<br />\r\nThe Samyang AF 85mm F1.4 RF is available now priced at &pound;599.99 / $699.99 in the UK and USA respectively.</p>\r\n\r\n<h2>Ease of Use</h2>\r\n\r\n<p><em>Weighing in at just 582 grams / 20.53 ounces and measuring almost 10cm in length, the Samyang AF 85mm F1.4 RF is quite a large and heavy lens given its modest focal length. Compared to it&#39;s most direct rival, the Canon RF 85mm F1.2L USM, the new Samyang lens is less than half the weight, so what you lose in light-gathering power, you more than make up for in terms of portability.</em></p>\r\n\r\n<p><em>As seen in the photos below, it&#39;s not a great match for the diminutive Canon EOS RP camera that we tested it with, especially when the large lens hood is attached. It would be more at home on the larger EOS R body, or the upcoming EOS R5 / R6 cameras.</em></p>\r\n\r\n<p>&nbsp;</p>\r\n');

-- --------------------------------------------------------

--
-- Структура таблицы `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `cat_name` varchar(100) NOT NULL,
  `keyword` varchar(100) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `categories`
--

INSERT INTO `categories` (`id`, `cat_name`, `keyword`, `description`) VALUES
(30, 'Photography', '', ''),
(31, 'Programming-language', '', ''),
(32, 'TESTCATEGORY', '#test', ''),
(29, 'Administrating', '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `date` timestamp NOT NULL DEFAULT current_timestamp(),
  `note` varchar(255) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `orders`
--

INSERT INTO `orders` (`id`, `user_id`, `date`, `note`, `product_id`) VALUES
(48, 39, '2020-06-05 15:28:51', 'Test note!!!', 2),
(49, 40, '2020-06-05 20:23:58', '', 3),
(50, 41, '2020-06-05 22:09:56', '', 2),
(51, 42, '2020-06-05 22:11:42', '', 2),
(52, 43, '2020-06-07 06:18:37', '', 3),
(53, 48, '2020-06-08 03:42:44', '', 2);

-- --------------------------------------------------------

--
-- Структура таблицы `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `tarif_name` varchar(50) NOT NULL,
  `price` float NOT NULL,
  `privilegies` varchar(200) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `product`
--

INSERT INTO `product` (`id`, `tarif_name`, `price`, `privilegies`) VALUES
(1, 'basic', 54.99, 'Team of 2-3 developers; Support after release 1 month; Free host for 3 months'),
(2, 'smart', 84.99, 'Team of 7-10 developers; Support after release 3 month; Free host for a year'),
(3, 'premium', 144.99, 'Full team developers; Support after release 6 month; 24/7 technical support; Free host for a 3 years; VIP-service');

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `login` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `name` varchar(50) NOT NULL,
  `role` enum('user','admin') DEFAULT 'user'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`id`, `login`, `password`, `email`, `name`, `role`) VALUES
(1, 'user1337', '$2y$10$TaYslXci1yKCFZT92b7c5e2yxUV2Uk8IY3ecG/74hH7/C5HieqL7C', 'admin@gmail.com', 'Jason', 'admin'),
(52, 'pi61_lva', '$2y$10$YMU39Efe6TeRqs49A3AIFuJ7zFfKSOFOuWvMlhND8qQFOxzjlciEC', 'pi61_lva@student.ztu.edu.ua', 'Vladislav', 'admin'),
(39, 'user2', '$2y$10$f3yMAxQR8dFMKwIfGlXOLu6z3M3iWwCp999F0fOQppIQNysbkpUtu', 'a@ya.ru', 'Vladislav', 'user'),
(40, 'Liza', '$2y$10$ioCr3465rO7IXbtB3qGvlewaLcA01QfIlhBT0YiPgMEMGPpG/89dK', 'elise@yandex.ru', 'Elizabeth', 'admin'),
(42, 'testuser1', '$2y$10$VINeLvV3y293cO1lyVQv2./VI3nBWbevb2XZQ0LI4mjksBgLzRLDe', 'dejavucsgoduck.com@gmail.com', 'Alex', 'user'),
(43, '4iL', '$2y$10$2cnnQ20CXwQuArBjVYZyNOAvE/VQd4nL.UdmHzqt.ErpA9sGwKLMS', '4iL@yahoo.com', 'Eugene', 'user'),
(45, 'TestUser', '$2y$10$FpDX0sacckroeN0Cb3UtEunaz7Q0l9XC9jrS1.Xsp3vlZHHdg8njm', 'admins@ukr.net', 'Uri', 'admin'),
(46, 'top_admin', '$2y$10$IK/jr2S8uENNcf776Atf1um2Xnz6vA4IYSzLebRJfh0OWFWEyrNle', 'new@gmail.com', 'Vladlen', 'admin'),
(48, 'forge', '$2y$10$rtkgiSJSNhpOTxkHccvwLeN3QLVVfqbbxaGHrPBuoTkEtckqFf9Aa', 'forge@gmail.com', 'Andrey', 'user'),
(50, 'pi61', '$2y$10$dSGeEDVab/SKGxDAsDwpbOK.0I5rah8ui/KHNmNH6p9dRhdXz1zJG', 'student@ukr.net', 'Admin', 'user');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `aboutus_clientfeedback`
--
ALTER TABLE `aboutus_clientfeedback`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `aboutus_info`
--
ALTER TABLE `aboutus_info`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `aboutus_ourclient`
--
ALTER TABLE `aboutus_ourclient`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `aboutus_team`
--
ALTER TABLE `aboutus_team`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `blogpost`
--
ALTER TABLE `blogpost`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `login` (`login`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `aboutus_clientfeedback`
--
ALTER TABLE `aboutus_clientfeedback`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `aboutus_info`
--
ALTER TABLE `aboutus_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `aboutus_ourclient`
--
ALTER TABLE `aboutus_ourclient`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT для таблицы `aboutus_team`
--
ALTER TABLE `aboutus_team`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблицы `blogpost`
--
ALTER TABLE `blogpost`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT для таблицы `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT для таблицы `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=91;
--
-- AUTO_INCREMENT для таблицы `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
