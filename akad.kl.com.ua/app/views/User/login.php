<!DOCTYPE html>
<html lang="en">
<head>
    <title>Login V19</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="/public/assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/public/assets/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="/public/assets/css/animate.css">
    <link rel="stylesheet" type="text/css" href="/public/assets/css/animsition.min.css">
    <link rel="stylesheet" type="text/css" href="/public/assets/css/util.css">
    <link rel="stylesheet" type="text/css" href="/public/assets/css/main.css">
</head>
<body>

<div class="limiter">
    <div class="container-login100">
        <div class="wrap-login100 p-l-55 p-r-55 p-t-65 p-b-50">
            <?php if (isset($_SESSION['error'])): ?>
                <div class="alert alert-danger">
                    <?php echo $_SESSION['error'];
                    unset($_SESSION['error']); ?>
                </div>
            <?php endif; ?>
            <?php if (isset($_SESSION['success'])): ?>
                <div class="alert alert-success">
                    <?php echo $_SESSION['success'];
                    unset($_SESSION['success']); ?>
                </div>
            <?php endif; ?>
            <form class="login100-form validate-form" method="post" data-toggle="validator" action="login" id="signup"
                  role="form">
					<span class="login100-form-title p-b-33">
						Account Log In
					</span>

                <div class=" validate-input form-group has-feedback">
                    <input class="input100 form-control" type="text" name="login" placeholder="Login"
                           value=""
                           required>

                </div>

                <div class=" rs1 validate-input form-group has-feedback">
                    <input class="input100 form-control" type="password" name="password" placeholder="Password"
                           required>
                </div>

                <div class="container-login100-form-btn m-t-20">
                    <button class="login100-form-btn">
                        Sign in
                    </button>
                </div>

            </form>
<!--            --><?// if (isset($_SESSION['form_data'])) {
//                unset($_SESSION['form_data']);
//            } ?>
        </div>
    </div>
</div>


<script type="text/javascript" src="/public/assets/js/jquery-2.1.4.min.js"></script>
<script src="/public/assets/js/validator.js"></script>
</body>
</html>