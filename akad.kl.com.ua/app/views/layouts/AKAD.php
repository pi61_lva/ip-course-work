<!DOCTYPE html>
<html>
<head>
    <?= $this->getMeta(); ?>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="/public/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="/public/assets/css/ionicons.min.css">
    <link rel="stylesheet" href="/public/assets/css/flexslider.css">
    <link rel="stylesheet" href="/public/assets/css/animsition.min.css">
    <link rel="stylesheet" href="/public/assets/css/animate.css">
    <link rel="stylesheet" href="/public/assets/css/style.css">
</head>
<body class="animsition">
<!-- HEADER  -->
<header class="main-header">
    <div class="container">
        <div class="logo">
            <a href="<?= PATH; ?> "><img src="/public/assets/img/logo.png" alt="logo"></a>
        </div>

        <div class="menu">
            <!-- desktop navbar -->
            <nav class="desktop-nav">
                <ul class="first-level">
                    <li><a href="<?= PATH; ?>" class="animsition-link">Home</a></li>
                    <li><a href="<?= PATH; ?>/about" class="animsition-link">about us</a></li>
                    <li><a href="<?= PATH; ?>/service" class="animsition-link">services</a></li>

                    <li><a href="<?= PATH; ?>/blog">blog</a>
                    </li>
                    <li><a>Account</a>
                        <ul class="second-level">
                            <?php if (!empty($_SESSION['user'])): ?>
                                <li><a href="" class="animsition-link">Welcome, <?
                                        echo $_SESSION['user']['name']; ?>
                                        !</a>
                                </li>
                                <li><a href="<?= PATH; ?>/user/logout" class="animsition-link">Log Out</a></li>
                            <?php else: ?>
                                <li><a href="<?= PATH; ?>/user/login" class="animsition-link">Log In</a></li>
                                <li><a href="<?= PATH; ?>/user/signup" class="animsition-link">Sign Up</a></li>
                            <?php endif; ?>
                            <? if (isset($_SESSION['user']) && $_SESSION['user']['role'] == 'admin'): ?>
                                <li><a href="<?= ADMIN ?>" class="animsition-link">Go to Admin Panel</a></li>
                            <?php endif; ?>
                        </ul>
                    </li>
                    <li><a href="<? PATH ?>/cart/view">&#128722</a></li>
                </ul>
            </nav>
            <nav class="mobile-nav"></nav>
            <div class="menu-icon">
                <div class="line"></div>
                <div class="line"></div>
                <div class="line"></div>
            </div>
        </div>
    </div>
</header>

<?= $content; ?>


<footer class="main-footer wow fadeInUp">
    <div class="container">
        <div class="col-md-8 col-sm-12">
            <div class="row">
                <nav class="footer-nav">
                    <ul>
                        <li><a href="<?= PATH; ?>" class="animsition-link link">Home</a></li>
                        <li><a href="<?= PATH; ?>/about" class="animsition-link link">about us</a></li>
                        <li><a href="<?= PATH; ?>/service" class="animsition-link link">services</a></li>
                        <li><a href="<?= PATH; ?>/blog" class="animsition-link link">blog</a></li>
                    </ul>
                </nav>
            </div>
        </div>

        <div class="col-md-4 col-sm-12" style="text-align:right">
            <div class="row">
                <div class="uppercase gray-text">
                    created by V.Labenskiy &copy;2020. all rights reserved.
                </div>
                <ul class="social-icons" style="margin-top:30px;float:right">
                    <li><a href="#"><i class="icon ion-social-facebook"></i></a></li>
                    <li><a href="#"><i class="icon ion-social-twitter"></i></a></li>
                    <li><a href="#"><i class="icon ion-social-youtube"></i></a></li>
                    <li><a href="#"><i class="icon ion-social-linkedin"></i></a></li>
                    <li><a href="#"><i class="icon ion-social-pinterest"></i></a></li>
                    <li><a href="#"><i class="icon ion-social-instagram"></i></a></li>
                </ul>
            </div>
        </div>
    </div>
</footer>

<!-- Modal -->
<div class="modal fade" id="cart" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Cart</h4>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <!--                <button type="button" class="btn btn-default" data-dismiss="modal">Продолжить покупки</button>-->
                <a href="cart/view" type="button" class="btn green">Pay</a>
                <!--                <button type="button" class="btn btn-danger" onclick="clearCart()">Очистить корзину</button>-->
            </div>
        </div>
    </div>
</div>


<script>
    var path = '<?= PATH;?>'
</script>
<script type="text/javascript" src="/public/assets/js/jquery-2.1.4.min.js"></script>
<script type="text/javascript" src="/public/assets/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/public/assets/js/typeahead.bundle.js"></script>
<!--category on homepage + porfolio-->
<script type="text/javascript" src="/public/assets/js/isotope.pkgd.min.js"></script>
<script type="text/javascript" src="/public/assets/js/jquery.flexslider.js"></script>
<script type="text/javascript" src="/public/assets/js/jquery.animsition.min.js"></script>
<script type="text/javascript" src="/public/assets/js/wow.min.js"></script>
<script type="text/javascript" src="/public/assets/js/main.js"></script>
<script src="/public/assets/js/validator.js"></script>
<!--CHOOSE CATEGORY -->
<script type="text/javascript" charset="utf-8">
    $(window).load(function () {
        new WOW().init();

        // initialise flexslider
        $('.site-hero').flexslider({
            animation: "fade",
            directionNav: false,
            controlNav: false,
            keyboardNav: true,
            slideToStart: 0,
            animationLoop: true,
            pauseOnHover: false,
            slideshowSpeed: 4000,
        });
        var $container = $('.portfolio_container');
        $container.isotope({
            filter: '*',
        });
        $('.portfolio_filter a').click(function () {
            $('.portfolio_filter .active').removeClass('active');
            $(this).addClass('active');

            var selector = $(this).attr('data-filter');
            $container.isotope({
                filter: selector,
                animationOptions: {
                    duration: 500,
                    animationEngine: "jquery"
                }
            });
            return false;
        });
    });

</script>

</body>
</html>
