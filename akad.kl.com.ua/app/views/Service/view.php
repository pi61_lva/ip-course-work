<!-- HERO SECTION  -->
<div class="site-hero_2">
    <div class="page-title">
        <div class="big-title montserrat-text uppercase">our services</div>
    </div>
</div>
<section>
    <div class="container">
        <div class="row">
            <div class="section-title">
                <span>what we do</span>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                    tempor incididunt ut labore et dolore magna aliqua.</p>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6 wow fadeInUp">
                <p style="margin-bottom:30px">
                    Whether it’s a pop-up meal, a visual study of a sector, or a training kit, all Creative Agency
                    projects are influenced by systems and people-oriented thinking, and end in targeted insights,
                    programs or products that move forward your organizational goals. All Creative Agency projects start
                    with a discovery phase in which we understand the core goals of your effort and apply the Creative
                    Agency approach to understanding the larger opportunities you might have. Creative Agency projects
                    can be categorized into three major types that are linked by a continuum of transforming information
                    into action and experience.
                </p>
                <div class="col-md-6">
                    <div class="row">
                        <ul class="list">
                            <li>Stunning on all screens</li>
                            <li>Easy to customize</li>
                            <li>Make a difference</li>
                            <li>Imagine and create</li>
                            <li>Unlimited possibilities</li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row">
                        <ul class="list">
                            <li>Remarkable style</li>
                            <li>Captivating presentations</li>
                            <li>Make a difference</li>
                            <li>Make a difference</li>
                            <li>Imagine and create</li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="col-md-6 wow fadeInUp" data-wow-delay=".1s">
                <img src="/public/assets/img/Services_Image1.jpg" alt="img" style="width:100%">
            </div>
        </div><!-- end row -->
    </div><!-- end container -->
</section>


<section>
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-12 wow fadeInUp">
                <div class="benefits_1_single">
                    <i class="icon ion-ios-pulse"></i>
                    <div class="title montserrat-text uppercase">keep pulse going</div>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                        tempor incididunt.
                    </p>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12 wow fadeInUp" data-wow-delay=".1s">
                <div class="benefits_1_single">
                    <i class="icon ion-ios-infinite-outline"></i>
                    <div class="title montserrat-text uppercase">unlimited options</div>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                        tempor incididunt.
                    </p>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12 wow fadeInUp" data-wow-delay=".2s">
                <div class="benefits_1_single">
                    <i class="icon ion-ios-lightbulb-outline"></i>
                    <div class="title montserrat-text uppercase">great ideas</div>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                        tempor incididunt.
                    </p>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12 wow fadeInUp" data-wow-delay=".3s">
                <div class="benefits_1_single">
                    <i class="icon ion-ios-settings"></i>
                    <div class="title montserrat-text uppercase">awesome support</div>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                        tempor incididunt.
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="pricing_plans">
    <div class="container">
        <div class="row">
            <div class="section-title">
                <span>pricing plans</span>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                    tempor incididunt ut labore et dolore magna aliqua.</p>
            </div>
        </div>
        <?php if ($products): ?>
            <div class="row">
                <?php foreach ($products as $product): ?>
                    <div class="col-md-4 col-sm-6 col-xs-12 wow fadeInUp">
                        <div class="pricing_plan">
                            <div class="plan_title montserrat-text uppercase"><?= $product->tarif_name; ?></div>
                            <div class="plan_price montserrat-text uppercase">$<?= $product->price; ?></div>
                            <ul class="list">
                                <? $priv = $product->privilegies;
                                $privi = explode(';', $priv);
                                foreach ($privi as $v): ?>
                                    <li><?= $v; ?></li>
                                <?php endforeach; ?>
                            </ul>
                            <a href="/cart/add?id=<?= $product->id ?>" id="productAdd" data-id="<?= $product->id; ?>"
                               class="btn green add-to-cart-link"><span>add to cart</span></a>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        <? endif; ?>
    </div>
</section>