<?= $this->getMeta(); ?>
<body class="animsition">
<!-- HERO SECTION  -->
<div class="site-hero_2">
    <div class="page-title">
        <div class="big-title montserrat-text uppercase">blog posts</div>
        <h2></h2>
    </div>
</div>

<section>
    <div class="container">
        <div class="row"> <!--виведення постів із БД-->
            <?php if (!empty($category)): ?>
                <div class="col-md-9 col-sm-12">
                    <?php foreach ($category as $categ): ?>
                        <!-- blog post -->
                        <div class="blog_post">
                            <div class="post_media">
                                <h3 class="montserrat-text uppercase"><?= $categ->title; ?></h3>
                            </div>
                            <div class="post_info">
                                <div class="post_date montserrat-text uppercase">
                                    Date: <?= $categ->publication_date; ?></div>
                                <span></span>
                                <span></span>
                            </div>
                            <p>
                                <?= $categ->short_text; ?>
                            </p>
                            <!--ПОСИЛАННЯ НА ПОСТИ --><a href="<?= PATH ?>/blog/<?= $categ->id; ?>"
                                                         class="link montserrat-text uppercase">continue
                                reading <i
                                  class="icon ion-arrow-right-c"></i></a>
                        </div>
                    <?php endforeach; ?>

                    <!-- pagination -->
                    <div class="blog_pagination wow fadeInUp">
                        <a href="" class="page">
                            <i class="icon ion-arrow-left-c prev"></i>
                            <span>previous</span>
                        </a>


                        <span class="divisor">/</span>
                        <a href="" class="page">
                            <span>next</span>
                            <i class="icon ion-arrow-right-c next"></i>
                        </a>
                    </div>
                </div><!-- end col -->

            <?php endif; ?>


            <div class="col-md-3">
                <div class="sidebar">
                    <div class="widget">
                        <form class="" action="<?PATH?>/search" method="get" autocomplete="off">
                            <input type="text" id="typeahead" name="s" placeholder="Input text..." required>
                            <button type="submit" class="btn green search">SEARCH</button>
                        </form>
                    </div>
                    <!-- categories -->
                    <?php if ($postCategory): ?>
                        <div class="widget wow fadeInUp">
                            <div class="widget_title">categories</div>
                            <ul class="list_2">
                                <?php foreach ($postCategory as $category): ?>
                                    <li><a
                                          href="<?PATH?>/category/<?= $category->cat_name; ?>"><?= $category->cat_name; ?>
                                            <span><?= $category->k_post; ?></span></a>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div><!-- end row -->
    </div><!-- end container -->
</section>


<script type="text/javascript" src="/public/assets/js/jquery-2.1.4.min.js"></script>
<script type="text/javascript" src="/public/assets/js/jquery.animsition.min.js"></script>
<script type="text/javascript" src="/public/assets/js/wow.min.js"></script>
<script type="text/javascript" src="/public/assets/js/main.js"></script>
<script type="text/javascript">
    $(window).load(function () {
        new WOW().init();
    });
</script>
</body>
</html>