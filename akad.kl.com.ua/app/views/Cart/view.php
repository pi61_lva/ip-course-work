<div class="prdt">
    <div class="container" style="padding-top: 8em;">
        <div class="prdt-top">
            <div class="col-md-12">
                <div class="product-one cart">
                    <div class="register-top heading">
                        <h2>Order registration</h2>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <?php if (isset($_SESSION['error'])): ?>
                                <div class="alert alert-danger" style="padding-left: 3em;">
                                    <?php echo $_SESSION['error'];
                                    unset($_SESSION['error']); ?>
                                </div>
                            <?php endif; ?>
                            <?php if (isset($_SESSION['success'])): ?>
                                <div class="alert alert-success">
                                    <?php echo $_SESSION['success'];
                                    unset($_SESSION['success']); ?>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                    <br><br>
                    <?php if (!empty($_SESSION['cart'])): ?>
                        <div class="table-responsive">
                            <table class="table table-hover table-striped">
                                <thead>
                                <tr>
                                    <th>Tariff name</th>
                                    <th>Price</th>
                                    <th><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></th>
                                </tr>
                                </thead>
                                <tbody class="uppercase">
                                <?php foreach ($_SESSION['cart'] as $id => $item): ?>
                                    <tr>
                                        <td><a href="product/<?= $item['alias'] ?>"><?= $item['title'] ?></a></td>
                                        <td><?= $item['price'] ?></td>
                                        <td><a href="/cart/delete/?id=<?= $id ?>"><span data-id="<?= $id ?>"
                                                                                        class="glyphicon glyphicon-remove text-danger del-item"
                                                                                        aria-hidden="true"></span></a>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-6 account-left">
                            <form method="post" action="<? PATH ?>/cart/checkout" role="form" data-toggle="validator">
                                <?php if (!isset($_SESSION['user'])): ?>
                                    <div class="form-group has-feedback">
                                        <label for="login">Login</label>
                                        <input type="text" name="login" class="form-control" id="login"
                                               placeholder="Login"
                                               value="<?= isset($_SESSION['form_data']['login']) ? $_SESSION['form_data']['login'] : '' ?>"
                                               required>
                                        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                    </div>
                                    <div class="form-group has-feedback">
                                        <label for="pasword">Password</label>
                                        <input type="password" name="password" class="form-control" id="pasword"
                                               placeholder="Password"
                                               value="<?= isset($_SESSION['form_data']['password']) ? $_SESSION['form_data']['password'] : '' ?>"
                                               data-minlength="6"
                                               data-error="Minimum of 6 characters" required>
                                        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                    <div class="form-group has-feedback">
                                        <label for="name">Name</label>
                                        <input type="text" name="name" class="form-control" id="name" placeholder="Name"
                                               value="<?= isset($_SESSION['form_data']['name']) ? $_SESSION['form_data']['name'] : '' ?>"
                                               required>
                                        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                    </div>
                                    <div class="form-group has-feedback">
                                        <label for="email">Email</label>
                                        <input type="email" name="email" class="form-control" id="email"
                                               placeholder="Email"
                                               value="<?= isset($_SESSION['form_data']['email']) ? $_SESSION['form_data']['email'] : '' ?>"
                                               required>
                                        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                    </div>
                                <?php endif; ?>
                                <div class="form-group">
                                    <label for="address">Note</label>
                                    <textarea name="note" class="form-control"></textarea>
                                </div>
                                <input type="checkbox" checked id="pay" style="display: none;" name="pay">
                                <button type="submit" class="btn green">
                                    Pay tariff
                                </button>
                            </form>
                            <?php if (isset($_SESSION['form_data'])) unset($_SESSION['form_data']); ?>
                        </div>
                    <?php else: ?>
                        <h3>The cart is empty &#9785; Choose a tariff for your...</h3>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>