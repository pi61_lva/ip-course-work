<?php if (!empty($_SESSION['cart'])): ?>
    <div class="table-responvie">
        <table class="table table-hover table-striped">
            <thead>
            <tr>
                <th>Tariff name</th>
                <th>Price</th>
            </tr>
            </thead>
            <tbody class="uppercase">
            <?php foreach ($_SESSION['cart'] as $id => $item): ?>
                <tr>
                    <td><?= $item['title'] ?></td>
                    <td>$<?= $item['price'] ?></td>
                    <!--                    <td>--><? //= $item['privilegies']; ?><!--</td>-->
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
<?php else: ?>
    <h3>The cart is empty &#9785; Choose a tariff for your...</h3>
<?php endif; ?>
