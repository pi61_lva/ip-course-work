<!-- HERO SECTION  -->
<div class="site-hero_2">
    <div class="page-title">
        <div class="big-title montserrat-text uppercase">about us</div>
    </div>
</div>
<?php if ($info): ?>
    <section>
        <div class="container">
            <div class="row">
                <?php foreach ($info as $inf): ?>
                    <div class="col-md-6">
                        <div class="section-title" style="text-align:left;float:left;width:100%;margin-bottom:0">
                            <span><?= $inf->section_title; ?></span>
                            <p class="montserrat-text uppercase"><?= $inf->small_title; ?></p>
                        </div>
                        <p><?= $inf->text; ?></p>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </section>
<?php endif; ?>

<section>
    <div class="container">
        <div class="row">
            <div class="section-title">
                <span>some benefits</span>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                    tempor incididunt ut labore et dolore magna aliqua.</p>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4 col-sm-6 col-xs-12 benefits_2_single wow fadeInUp">
                <i class="icon ion-iphone"></i>
                <span class="title montserrat-text uppercase">fully responsive</span>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                    quis nostrud exercitation. </p>
            </div>

            <div class="col-md-4 col-sm-6 col-xs-12 benefits_2_single wow fadeInUp" data-wow-delay=".1s">
                <i class="icon ion-ios-infinite-outline"></i>
                <span class="title montserrat-text uppercase">unlimited options</span>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                    quis nostrud exercitation. </p>
            </div>

            <div class="col-md-4 col-sm-6 col-xs-12 benefits_2_single wow fadeInUp" data-wow-delay=".2s">
                <i class="icon ion-social-wordpress-outline"></i>
                <span class="title montserrat-text uppercase">wordpress</span>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                    quis nostrud exercitation. </p>
            </div>

            <div class="col-md-4 col-sm-6 col-xs-12 benefits_2_single wow fadeInUp" data-wow-delay=".3s">
                <i class="icon ion-ios-cart-outline"></i>
                <span class="title montserrat-text uppercase">e-commerce</span>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                    quis nostrud exercitation. </p>
            </div>

            <div class="col-md-4 col-sm-6 col-xs-12 benefits_2_single wow fadeInUp" data-wow-delay=".4s">
                <i class="icon ion-ios-settings"></i>
                <span class="title montserrat-text uppercase">customizable design</span>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                    quis nostrud exercitation. </p>
            </div>

            <div class="col-md-4 col-sm-6 col-xs-12 benefits_2_single wow fadeInUp" data-wow-delay=".5s">
                <i class="icon ion-settings"></i>
                <span class="title montserrat-text uppercase">support</span>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                    quis nostrud exercitation. </p>
            </div>
        </div>
    </div>
</section>

<?php if ($teams): ?>
    <section>

        <div class="container">
            <div class="row">
                <div class="section-title">
                    <span>the dream team</span>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                        tempor incididunt ut labore et dolore magna aliqua.</p>
                </div>
            </div>

            <div class="row">
                <?php foreach ($teams as $team): ?>
                    <div class="col-md-3 col-sm-6 col-xs-12 wow fadeInUp">
                        <div class="team_member">
                            <img src="/public/assets/img/<?= $team->img_src; ?>" alt="team image">
                            <div class="team_member_hover">
                                <div class="team_member_info">
                                    <div class="team_member_name"><?= $team->name; ?></div>
                                    <div class="team_member_job"><?= $team->position; ?></div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>

        </div>
    </section>

<?php endif; ?>
<!-- light gray section -->
<div class="container">
    <div class="light-gray-section wow fadeInUp" style="padding:15px 30px;">
        <div class="row">
            <p class="italic" style="float:left;line-height:50px;margin:0">
                Our team creates websites that are used by millions of users
            </p>
            <a href="" class="btn green" style="float:right"><span>view portfolio</span></a>
        </div>
    </div>
</div>


<section>
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="testimonials wow fadeInUp">
                    <?php if ($clientFeed): ?>
                        <?php foreach ($clientFeed as $clientfeed): ?>
                            <ul class="slides">

                                <li class="testimonials_single">
                                    <div class="author_pic">
                                        <img src="/public/assets/img/<?= $clientfeed->client_img; ?>"
                                             alt="testimonial image">
                                    </div>
                                    <p>
                                        <?= $clientfeed->text; ?>
                                    </p>
                                    <div class="author_name"><?= $clientfeed->client_name; ?></div>
                                </li>
                            </ul>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </div>
            </div>

            <div class="col-md-6 clients">
                <div class="row">
                    <?php if ($clients): ?>
                        <?php foreach ($clients as $client): ?>
                            <div class="col-md-4 col-sm-6 col-xs-12 wow fadeInUp">
                                <div class="clients_single">
                                    <a href="<?= $client->webpage; ?>" target="_blank">
                                        <img src="/public/assets/img/<?= $client->logo; ?>" alt="client logo">
                                    </a>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </div>
            </div>

        </div>
    </div>
</section>