<section class="content-header">
    <h1>
        List of users
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= ADMIN ?>"><i class="fa fa-dashboard"></i>Main page</a></li>
        <li class="active">List of users</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Login</th>
                                <th>E-mail</th>
                                <th>Role</th>
                                <th>User name</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <? foreach ($users as $user): ?>
                                <tr>
                                    <td><?= $user->id ?></td>
                                    <td><?= $user->login ?></td>
                                    <td><?= $user->email ?></td>
                                    <td><?= $user->role ?></td>
                                    <td><?= $user->name ?></td>
                                    <td><a href="<?= ADMIN; ?>/user/edit?id=<?= $user->id; ?>">
                                            <i class="fa fa-fw fa-pencil"></i><a class="delete"
                                                                                 href="<?= ADMIN; ?>/user/delete?id=<?= $user->id; ?>"><i
                                                  class="fa fa-fw fa-close text-danger"></i></a></td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
