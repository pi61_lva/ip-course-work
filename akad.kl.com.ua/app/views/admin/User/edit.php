<section class="content-header">
    <h1>
        Editing user profile: <i><strong><?= $user->login; ?></strong></i>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= ADMIN ?>"><i class="fa fa-dashboard"></i>Main page</a></li>
        <li><a href="<?= ADMIN ?>/user"><i class="fa fa-dashboard"></i>List of users</a></li>
        <li class="active">Editing user profile</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <form action="<?= ADMIN ?>/user/edit" method="post" class="" data-toggle="validator">
                    <div class="box-body">
                        <div class="form-group has-feedback">
                            <label for="login">Login</label>
                            <input type="text" class="form-control has-feedback" name="login" id="login"
                                   value="<?= h($user->login); ?>" placeholder="Login" required>
                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                        </div>
                        <div class="form-group has-feedback">
                            <label for="password">Password</label>
                            <input type="password" class="form-control has-feedback" name="password" id="password"
                                   placeholder="Enter your password if you want to change it">
                        </div>
                        <div class="form-group has-feedback">
                            <label for="email">Email</label>
                            <input type="email" class="form-control has-feedback" name="email" id="email"
                                   value="<?= h($user->email); ?>" required>
                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                        </div>
                        <div class="form-group has-feedback">
                            <label for="name">Name</label>
                            <input type="text" class="form-control has-feedback" name="name" id="name"
                                   value="<?= h($user->name); ?>" required>
                        </div>
                        <div class="form-group has-feedback">
                            <label>Role</label>
                            <select name="role" id="role" class="form-control">
                                <option value="user" <?php if ($user->role == 'user') echo ' selected'; ?>>User</option>
                                <option value="admin" <?php if ($user->role == 'admin') echo ' selected'; ?>>
                                    Administrator
                                </option>
                            </select>
                        </div>
                    </div>
                    <div class="box-footer">
                        <input type="hidden" name="id" value="<?= $user->id; ?>">
                        <button type="submit" class="btn btn-primary">Edit user profile</button>
                    </div>
                </form>
            </div>
            <h3>User tariff plan</h3>
            <div class="box">
                <div class="box-body">
                    <?php if ($orders): ?>
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>Subscribe ID</th>
                                    <th>Tariff name</th>
                                    <th>Date of activation of the tariff planя</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($orders as $order): ?>
                                    <tr>
                                        <td><?= $order['id']; ?></td>
                                        <td><?= $order['tarif_name'] ?></td>
                                        <td><?= $order['date']; ?></td>

                                    </tr>
                                <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    <?php else: ?>
                        <p class="text-danger">User does not have an active tariff plan...</p>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>

</section>
<!-- /.content -->
