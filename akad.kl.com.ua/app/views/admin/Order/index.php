<section class="content-header">
    <h1>
        Dashboard
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= ADMIN ?>"><i class="fa fa-dashboard"></i>Main page</a></li>
        <li class="active">Subsribers list</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>User login</th>
                                <th>User name</th>
                                <th>Order date</th>
                                <th>Tariff name</th>
                            </tr>
                            </thead>
                            <tbody>
                            <? foreach ($orders as $order): ?>
                                <tr>
                                    <td><?= $order['id'] ?></td>
                                    <td><?= $order['login'] ?></td>
                                    <td><?= $order['name'] ?></td>
                                    <td><?= $order['date'] ?></td>
                                    <td style="text-transform: uppercase;"><?= $order['tarif_name'] ?></td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
