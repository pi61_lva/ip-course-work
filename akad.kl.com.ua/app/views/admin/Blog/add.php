<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        New article
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= ADMIN; ?>"><i class="fa fa-dashboard"></i>Main page</a></li>
        <li><a href="<?= ADMIN; ?>/blog">List of articles</a></li>
        <li class="active">New article</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <form action="<?= ADMIN; ?>/blog/add" method="post" data-toggle="validator">
                    <div class="box-body">
                        <div class="form-group has-feedback">
                            <label for="title">Article title</label>
                            <input type="text" name="title" class="form-control" id="title" placeholder="Article title"
                                   value="<?php echo isset($_SESSION['data']['title']) ? $_SESSION['data']['title'] : null; ?>"
                                   required>
                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                        </div>

                        <div class="form-group">
                            <label for="category_id">Choose category:</label>
                            <select class="form-control" name="category_id">
                                <?php foreach ($category as $cat): ?>
                                    <option name="category_id"
                                            value="<?= (int)$cat['id']; ?>"><?= $cat['cat_name']; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="short_text">Short text (preview)</label>
                            <textarea class="form-control" id="editor1" name="short_text"
                                      id="short_text"
                                      style="height: 100px;"><?php echo isset($_SESSION['data']['short_text']) ? $_SESSION['data']['short_text'] : null; ?></textarea>
                        </div>

                        <div class="form-group">
                            <label for="all_text">All text in article</label>
                            <textarea class="form-control" name="all_text"
                                      id="editor2"><?php echo isset($_SESSION['data']['all_text']) ? $_SESSION['data']['all_text'] : null; ?></textarea>
                        </div>


                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-success">Add new article</button>
                    </div>
                </form>
                <?php if (isset($_SESSION['data'])) unset($_SESSION['data']); ?>
            </div>
        </div>
    </div>

</section>
<!-- /.content -->