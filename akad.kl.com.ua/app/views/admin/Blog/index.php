<link rel="stylesheet" href="/public/assets/css/blog.css"/>
<section class="content-header">
    <h1>
        List of articles
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= ADMIN ?>"><i class="fa fa-dashboard"></i>Main page</a></li>
        <li class="active">List of articles</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Title</th>
                                <th>Category</th>
                                <th>Short text</th>
                                <th style="width: 50%;">All text</th>
                                <th>Publication date</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <? foreach ($articles as $article): ?>
                                <tr>
                                    <td><?= $article['id'] ?></td>
                                    <td><?= $article['title'] ?></td>
                                    <td><?= $article['cat_name'] ?></td>
                                    <td><?= $article['short_text'] ?></td>
                                    <td><?= $article['all_text'] ?></td>
                                    <td><?= $article['publication_date'] ?></td>
                                    <td><a href="<?= ADMIN; ?>/blog/edit?id=<?= $article['id']; ?>"><i
                                              class="fa fa-fw fa-pencil"></i></a> <a class="delete"
                                                                                  href="<?= ADMIN; ?>/blog/delete?id=<?= $article['id']; ?>"><i
                                              class="fa fa-fw fa-close text-danger"></i></a></td>

                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
