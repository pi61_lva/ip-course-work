<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Article editing: <?= $post->title; ?>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= ADMIN; ?>"><i class="fa fa-dashboard"></i>Main page</a></li>
        <li><a href="<?= ADMIN; ?>/blog">List of articles</a></li>
        <li class="active">Article editing</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <form action="<?= ADMIN; ?>/blog/edit" method="post" data-toggle="validator">
                    <div class="box-body">
                        <div class="form-group has-feedback">
                            <label for="title">Article title</label>
                            <input type="text" name="title" class="form-control" id="title" placeholder="Article title"
                                   value="<?= h($post->title); ?>"
                                   required>
                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                        </div>

                        <div class="form-group">
                            <label for="category_id">Choose category:</label>
                            <select class="form-control" name="category_id">
                                <?php foreach ($category as $cat): ?>
                                    <option name="category_id"
                                            value="<?= (int)$cat['id']; ?>"><?= $cat['cat_name']; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="short_text">Short text (preview)</label>
                            <textarea class="form-control" id="editor1" name="short_text"
                                      id="short_text"
                                      style="height: 100px;"><?= h($post->short_text); ?></textarea>
                        </div>

                        <div class="form-group">
                            <label for="all_text">All text in article</label>
                            <textarea class="form-control" name="all_text"
                                      id="editor2"><?= h($post->all_text); ?></textarea>
                        </div>
                    </div>
                    <div class="box-footer">
                        <div class="box-footer">
                            <input type="hidden" name="id" value="<?= $post->id; ?>">
                            <button type="submit" class="btn btn-success">Save changes</button>
                        </div>
                </form>
            </div>
        </div>
    </div>

</section>
<!-- /.content -->