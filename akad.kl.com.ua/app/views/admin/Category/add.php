<section class="content-header">
    <h1>
        New category
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= ADMIN ?>"><i class="fa fa-fw fa-close text-danger"></i>Main page</a></li>
        <li><a href="<?=ADMIN ?>/category">List of categories</a></li>
        <li class="active"><a href="<?=ADMIN ?>/category">New category</a></li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <?php if (isset($_SESSION['error'])): ?>
                <div class="alert alert-danger">
                    <?php echo $_SESSION['error'];
                    unset($_SESSION['error']); ?>
                </div>
            <?php endif; ?>
            <?php if (isset($_SESSION['success'])): ?>
                <div class="alert alert-success">
                    <?php echo $_SESSION['success'];
                    unset($_SESSION['success']); ?>
                </div>
            <?php endif; ?>
        </div>
        <div class="col-md-12">
            <div class="box">
                <form action="<?= ADMIN; ?>/category/add" method="post" data-toggle="validator">
                    <div class="box-body">
                        <div class="form-group has-feedback">
                            <label for="title">Category name</label>
                            <input type="text" name="cat_name" class="form-control" id="title"
                                   placeholder="Category name" required>
                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                        </div>

                        <div class="form-group">
                            <label for="keywords">Keywords</label>
                            <input type="text" name="keyword" class="form-control" id="keyword"
                                   placeholder="Keywords">
                        </div>
                        <div class="form-group">
                            <label for="description">Description</label>
                            <input type="text" name="description" class="form-control" id="description"
                                   placeholder="Description">
                        </div>
                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-success">Add new category</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

</section>
<!-- /.content -->
