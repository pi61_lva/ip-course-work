<section class="content-header">
    <h1>
        Editing a <?= $category->cat_name; ?> category
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= ADMIN; ?>"><i class="fa fa-fw fa-close text-danger"></i>Main page</a></li>
        <li><a href="<?= ADMIN; ?>/admin/category">Edit category</a></li>
        <li class="active"><a href="<?=ADMIN; ?>/category"><?= $category->cat_name; ?></a></li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <?php if (isset($_SESSION['error'])): ?>
                <div class="alert alert-danger">
                    <?php echo $_SESSION['error'];
                    unset($_SESSION['error']); ?>
                </div>
            <?php endif; ?>
            <?php if (isset($_SESSION['success'])): ?>
                <div class="alert alert-success">
                    <?php echo $_SESSION['success'];
                    unset($_SESSION['success']); ?>
                </div>
            <?php endif; ?>
        </div>
        <div class="col-md-12">
            <div class="box">
                <form action="<?= ADMIN; ?>/category/edit" method="post" data-toggle="validator">
                    <div class="box-body">
                        <div class="form-group has-feedback">
                            <label for="title">Category name</label>
                            <input type="text" name="cat_name" class="form-control" id="title"
                                   placeholder="Category name" required value="<?= h($category->cat_name); ?>">
                        </div>
                        <div class="form-group">
                            <label for="keywords">Keyword</label>
                            <input type="text" name="keyword" class="form-control" id="keyword"
                                   placeholder="Keywords" value="<?= h($category->keyword); ?>">
                        </div>
                        <div class="form-group">
                            <label for="description">Description</label>
                            <input type="text" name="description" class="form-control" id="description"
                                   placeholder="Description" value="<?= h($category->description); ?>">
                        </div>
                    </div>
                    <div class="box-footer">
                        <input type="hidden" name="id" value="<?= h($category->id); ?>">
                        <button type="submit" class="btn btn-success">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

</section>
<!-- /.content -->
