<section class="content-header">
    <h1>
        List of categories
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= ADMIN ?>"><i class="fa fa-fw fa-close text-danger"></i>Main page</a></li>
        <li class="active">List of categories</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <?php if (isset($_SESSION['error'])): ?>
                <div class="alert alert-danger">
                    <?php echo $_SESSION['error'];
                    unset($_SESSION['error']); ?>
                </div>
            <?php endif; ?>
            <?php if (isset($_SESSION['success'])): ?>
                <div class="alert alert-success">
                    <?php echo $_SESSION['success'];
                    unset($_SESSION['success']); ?>
                </div>
            <?php endif; ?>
        </div>
        <div class="col-md-12">
            <div class="box">
                <div class="box-body">
                    <?php


                    $delete = '<i class="fa fa-fw fa-close"></i>';

                    ?>
                    <?php foreach ($category as $categ):
                        $delete = '<a href="' . ADMIN . '/category/delete?id=' . $categ->id . '" class="delete"><i class="fa fa-fw fa-close text-danger"></i></a>'; ?>
                        <p class="item-p">
                            <a class="list-group-item" href="<?= ADMIN; ?>/category/edit?id=<?= $categ->id; ?>">
                                <?= $categ->cat_name; ?></a>
                            <span><?= $delete; ?></span>
                        </p>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
</section>
