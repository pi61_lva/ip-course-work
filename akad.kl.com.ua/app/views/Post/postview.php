<!-- HERO SECTION  -->
<?php if ($post): ?>
<div class="site-hero_2">
    <div class="page-title">
        <div class="big-title montserrat-text uppercase"><?= $post->title; ?></div>
    </div>
</div>


<section>

    <div class="container">
        <div class="row">
            <div class="col-md-9 col-sm-12">
                <div class="single_post">
                    <div class="post_title">

                        <span class="post_date">Date : <?= $post->publication_date; ?></span>
                    </div>
                    <div>
                        <p>
                            <!--LONG TEXT-->
                            <?= $post->all_text; ?>
                        </p>
                    </div>
                    <div class="row">
                        <div class="col-md-6">

                        </div>
                    </div>
                </div>

                <div class="pages_pagination">
                    <a href="<?= PATH; ?>/blog" class="all"><i class="icon ion-grid"></i></a>
                </div>
            </div><!-- end col -->

            <div class="col-md-3">
                <div class="sidebar">
                    <div class="widget">
                        <form class="" action="<?= PATH ?>/search" method="get" autocomplete="off">
                            <input type="text" id="typeahead" name="s" placeholder="Input text..." required>
                            <button type="submit" class="btn green search">SEARCH</button>
                        </form>

                    </div>
                    <?php if ($postCategory): ?>
                        <div class="widget wow fadeInUp">
                            <div class="widget_title">categories</div>
                            <ul class="list_2">
                                <?php foreach ($postCategory as $category): ?>
                                    <li><a
                                          href="<?= PATH ?>/category/<?= $category->cat_name; ?>"><?= $category->cat_name; ?></a>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div><!-- end row -->
    </div><!-- end container -->
    <? endif; ?>
</section>

