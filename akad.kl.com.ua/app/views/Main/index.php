</header>
<div class="site-hero">
    <ul class="slides">
        <li>
            <div><span class="small-title uppercase montserrat-text">we're</span></div>
            <div class="big-title uppercase montserrat-text">digital agency</div>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                tempor incididunt ut labore et dolore magna aliqua.</p>
        </li>
        <li>
            <div><span class="small-title uppercase montserrat-text">we do</span></div>
            <div class="big-title uppercase montserrat-text">creative stuff</div>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                consequat. </p>
        </li>
    </ul>
</div>
<div class="container">
    <div class="agency">
        <div class="col-md-5 col-sm-12">
            <div class="row">
                <img src="/public/assets/img/agency.jpg" alt="image">
            </div>
        </div>
        <div class="col-md-offset-1 col-md-6 col-sm-12">
            <div class="row">
                <div class="section-title">
                    <span>history of agency</span>
                </div>
                <p>
                    AKAD is a California-based digital agency founded in 2010. Their team works with enterprise clients
                    on their UX design systems, design, development, and branding needs. AKAD developed a website
                    focused on employee benefits for a Fortune 500 media group. Isadora began their work by assessing
                    the company's existing site and researching how other companies designed sites geared toward
                    employee benefits.
                </p>
                <a href="<?= PATH?>/about" class="btn green" style="float:right;margin-top:30px"><span>read more</span></a>
            </div>
        </div>
    </div>
</div>

<!-- WHY CHOOSE US -->
<section class="services">
    <div class="container">
        <div class="row">
            <div class="section-title">
                <span>why choose us</span>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                    tempor incididunt ut labore et dolore magna aliqua.</p>
            </div>
        </div>

        <div class="col-md-7 col-sm-12 services-left wow fadeInUp">
            <div class="row" style="margin-bottom:50px">
                <div class="col-md-6 col-sm-12">
                    <div class="row">
                        <i class="icon ion-ios-infinite-outline"></i>
                        <span class="montserrat-text uppercase service-title">unlimited options</span>
                        <ul>
                            <li>branding</li>
                            <li>design &amp; copywriting</li>
                            <li>concept development</li>
                            <li>user experience</li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12">
                    <div class="row">
                        <i class="icon ion-ios-shuffle"></i>
                        <span class="montserrat-text uppercase service-title">design &amp; development</span>
                        <ul>
                            <li>branding</li>
                            <li>design &amp; copywriting</li>
                            <li>concept development</li>
                            <li>user experience</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-sm-12">
                    <div class="row">
                        <i class="icon ion-ios-cart-outline"></i>
                        <span class="montserrat-text uppercase service-title">e-commerce</span>
                        <ul>
                            <li>branding</li>
                            <li>design &amp; copywriting</li>
                            <li>concept development</li>
                            <li>user experience</li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12">
                    <div class="row">
                        <i class="icon ion-ios-settings"></i>
                        <span class="montserrat-text uppercase service-title">customizable design</span>
                        <ul>
                            <li>branding</li>
                            <li>design &amp; copywriting</li>
                            <li>concept development</li>
                            <li>user experience</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-5 col-sm-12 services-right wow fadeInUp" data-wow-delay=".1s">
            <div class="row">
                <img src="/public/assets/img/serv.jpg" alt="image">
            </div>
        </div>

    </div>
</section>

<!-- PORTFOLIO -->
<section class="portfolio">
    <div class="container">
        <div class="row">
            <div class="section-title">
                <span>our portfolio</span>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                    tempor incididunt ut labore et dolore magna aliqua.</p>
            </div>
        </div>
        <div class="col-md-3">
            <div class="row categories-grid wow fadeInLeft">
                <span class="montserrat-text uppercase">choose category</span>

                <nav class="categories">
                    <ul class="portfolio_filter">
                        <li><a href="" class="active" data-filter="*">all</a></li>
                        <li><a href="" data-filter=".photography">photography</a></li>
                        <li><a href="" data-filter=".web">webdesign</a></li>
                        <li><a href="" data-filter=".logo">logo</a></li>
                        <li><a href="" data-filter=".graphics">graphics</a></li>
                        <li><a href="" data-filter=".ads">advertising</a></li>
                        <li><a href="" data-filter=".fashion">fashion</a></li>
                    </ul>
                </nav>
            </div>
        </div>
        <div class="col-md-9">
            <div class="row portfolio_container">
                <div class="col-md-4 photography">
                    <a href="single-project.html" class="portfolio_item work-grid wow fadeInUp">
                        <img src="/public/assets/img/work-1.jpg" alt="image">
                        <div class="portfolio_item_hover">
                            <div class="item_info">
                                <span>Brave man</span>
                                <em>photography</em>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-md-4 fashion logo">
                    <a href="single-project.html" class="portfolio_item work-grid wow fadeInUp" data-wow-delay=".2s">
                        <img src="/public/assets/img/work-2.jpg" alt="image">
                        <div class="portfolio_item_hover">
                            <div class="item_info">
                                <span>super man</span>
                                <em>photography</em>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-md-4 ads graphics">
                    <a href="single-project.html" class="portfolio_item work-grid wow fadeInUp" data-wow-delay=".3s">
                        <img src="/public/assets/img/work-3.jpg" alt="image">
                        <div class="portfolio_item_hover">
                            <div class="item_info">
                                <span>bat man</span>
                                <em>photography</em>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-md-4 fashion ads">
                    <a href="single-project.html" class="portfolio_item work-grid wow fadeInUp" data-wow-delay=".4s">
                        <img src="/public/assets/img/work-4.jpg" alt="image">
                        <div class="portfolio_item_hover">
                            <div class="item_info">
                                <span>spider man</span>
                                <em>photography</em>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-md-4 graphics ads">
                    <a href="single-project.html" class="portfolio_item work-grid wow fadeInUp" data-wow-delay=".5s">
                        <img src="/public/assets/img/work-5.jpg" alt="image">
                        <div class="portfolio_item_hover">
                            <div class="item_info">
                                <span>iron man</span>
                                <em>photography</em>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-md-4 logo web photography">
                    <a href="single-project.html" class="portfolio_item work-grid wow fadeInUp" data-wow-delay=".6s">
                        <img src="/public/assets/img/work-6.jpg" alt="image">
                        <div class="portfolio_item_hover">
                            <div class="item_info">
                                <span>iron man</span>
                                <em>photography</em>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-md-4 ads graphics">
                    <a href="single-project.html" class="portfolio_item work-grid wow fadeInUp" data-wow-delay=".7s">
                        <img src="/public/assets/img/work-7.jpg" alt="image">
                        <div class="portfolio_item_hover">
                            <div class="item_info">
                                <span>iron man</span>
                                <em>photography</em>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-md-4 web fashion photography">
                    <a href="single-project.html" class="portfolio_item work-grid wow fadeInUp" data-wow-delay=".8s">
                        <img src="/public/assets/img/work-8.jpg" alt="image">
                        <div class="portfolio_item_hover">
                            <div class="item_info">
                                <span>iron man</span>
                                <em>photography</em>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-md-4 fashion logo">
                    <a href="single-project.html" class="portfolio_item work-grid wow fadeInUp" data-wow-delay=".9s">
                        <img src="/public/assets/img/work-9.jpg" alt="image">
                        <div class="portfolio_item_hover">
                            <div class="item_info">
                                <span>iron man</span>
                                <em>photography</em>
                            </div>
                        </div>
                    </a>
                </div>
            </div>

        </div>

    </div>

</section>

