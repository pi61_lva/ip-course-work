<?php


namespace app\models;


class Category extends AppModel
{
    public $attributes = [
        'cat_name' => '',
        'keyword' => '',
        'description' => ''
    ];

    public $rules = [
        'required' => [
            ['title'],
        ]
    ];
}