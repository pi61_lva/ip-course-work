<?php


namespace app\models\admin;


use app\models\AppModel;

class Blog extends AppModel
{
    public $attributes = [
        'title' => '',
        'category_id' => '',
        'short_text' => '',
        'all_text' => '',

    ];
    public $rules = [
        'required' => [
            ['title'],
            ['category_id'],
            ['short_text'],
            ['all_text'],
        ],
        'integer' => [
            ['category_id'],
        ]
    ];
}