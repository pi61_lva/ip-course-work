<?php


namespace app\models\admin;


class User extends \app\models\User
{
    public $attributes = [
        'id' => '',
        'login' => '',
        'password' => '',
        'email' => '',
        'name' => '',
        'role' => ''
    ];

    public $rules = [
        'required' => [
            ['login'],
            ['name'],
            ['email'],
            ['role'],
        ],
        'email' => [
            ['email'],
        ],
    ];

    public function checkUnique()
    {
        $user = \R::findOne('user', '(login = ? OR email = ?) AND id <> ?',
            [$this->attributes['login'], $this->attributes['email'], $this->attributes['id']]);
        if ($user) {
            if ($user->login == $this->attributes['login']) {
                $this->errors['unique'][] = 'This login is already use';
            }
            if ($user->email == $this->attributes['email']) {
                $this->errors['unique'][] = 'This email is already use';
            }
            return false;
        }
        return true;
    }

}