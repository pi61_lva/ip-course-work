<?php


namespace app\models;


use akad\App;
use Swift_Message;

class Order extends AppModel
{

    public static function saveOrder($data)
    {
        $order = \R::dispense('orders');
        $order->user_id = $data['user_id'];
        $order->note = $data['note'];

//        $order->price = $_SESSION['cart.price'];
        foreach ($_SESSION['cart'] as $product_id => $product) {
            $order->product_id = (int)$product_id;
        }
        $order_id = \R::store($order);
        return $order_id;
    }


    public static function mailOrder($order_id, $user_email)
    {
        try {
            $transport = (new \Swift_SmtpTransport(App::$app->getProperty('smtp_host'), App::$app->getProperty('smtp_port'),
                App::$app->getProperty('smtp_protocol')))
                ->setUsername(App::$app->getProperty('smtp_login'))
                ->setPassword(App::$app->getProperty('smtp_password'));

            $mailer = new \Swift_Mailer($transport);
            /*create message*/
            ob_start();
            require APP . '/views/Mail/mail_order.php';
            $body = ob_get_clean();
            $message_client = (new Swift_Message("Confirmation of registration of the tariff plan"))
                ->setFrom([App::$app->getProperty('smtp_login') => App::$app->getProperty('site_name')])
                ->setTo($user_email)
                ->setBody($body, 'text/html');

            $message_admin = (new Swift_Message('AKAD'))
                ->setFrom([App::$app->getProperty('smtp_login') => App::$app->getProperty('site_name')])
                ->setTo(App::$app->getProperty('admin_email'))
                ->setBody($body, 'text/html');
            $result = $mailer->send($message_client);
            $result = $mailer->send($message_admin);
        } catch (\Exception $e) {

        }
//        unset($_SESSION['cart']);
        $_SESSION['success'] = 'Thank you for choosing our agency. We &#10084; our clients';
    }
}