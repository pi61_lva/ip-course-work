<?php


namespace app\models;


class Cart extends AppModel
{

    public function addToCart($product)
    {
        if ($product) {
            $ID = "{$product->id}";
            $title = "{$product->tarif_name}";
            $price = "{$product->price}";
        }
        if (!isset($_SESSION['cart'])) {
            if (!isset($_SESSION['cart'][$ID])) {
                $_SESSION['cart'][$ID] = [
                    'title' => $product->tarif_name,
                    'price' => $product->price,
                    'privilegies' => $product->privilegies,
                ];
            }
        } else {
            unset($_SESSION['cart']);
            $_SESSION['cart'][$ID] = [
                'title' => $product->tarif_name,
                'price' => $product->price,
                'privilegies' => $product->privilegies,
            ];
        }
    }

    public function deleteItem($id)
    {
        unset($_SESSION['cart'][$id]);
    }
}