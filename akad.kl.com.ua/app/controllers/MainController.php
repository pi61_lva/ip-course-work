<?php


namespace app\controllers;

use akad\Cache;

class MainController extends AppController
{

    public function indexAction()
    {
        $this->setMeta('AKAD Home Page', 'AKAD Home Page', 'AKAD, Creative Agency');
    }
}