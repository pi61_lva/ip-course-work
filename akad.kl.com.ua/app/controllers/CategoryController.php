<?php


namespace app\controllers;


class CategoryController extends AppController
{
    public function viewAction()
    {
        $alias = $this->route['post'];
        $category = \R::findAll('blogpost', 'inner join categories c on blogpost.category_id = c.id where cat_name = ? order by publication_date DESC', [$alias]);
        $postCategory = \R::findAll('categories');
        if (!$category) {
            throw new \Exception('Сторінка не знайдено', 404);
        }
        $this->setMeta('AKAD BLOG');
        $this->set(compact('category', 'postCategory'));

    }
}