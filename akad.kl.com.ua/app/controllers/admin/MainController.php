<?php


namespace app\controllers\admin;


class MainController extends AppController
{
    public function indexAction()
    {
        $countSubscibers = \R::count('orders');
        $countUsers = \R::count('user');
        $countPost = \R::count('blogpost');
        $countPostCategory = \R::count('categories');
        $this->set(compact('countSubscibers', 'countPostCategory', 'countUsers', 'countPost'));

        $this->setMeta('AKAD Admin Panel');
    }
}