<?php


namespace app\controllers\admin;


use app\models\User;

class UserController extends AppController
{
    public function indexAction()
    {
        $users = \R::findAll('user');
        $this->setMeta('List of users');
        $this->set(compact('users'));
    }

    public function loginAdminAction()
    {
        if (!empty($_POST)) {
            $user = new User();
            if ($user->login(true)) {
//                $_SESSION['success'] = 'Welcome to admin panel';
            } else {
                $_SESSION['error'] = 'Login/Password was wrong';
            }
            if (User::isAdmin()) {
                redirect(ADMIN);
            } else {
                redirect();
            }
        }
        $this->layout = 'login';
    }

    public function addAction()
    {
        $this->setMeta("New user");
    }

    public function deleteAction()
    {
        $id = $this->getRequestID();
        $errors = '';

        if ($errors) {
            $_SESSION['error'] = $errors;
            redirect();
        }
        $user = \R::load('user', $id);
        \R::trash($user);
        $_SESSION['success'] = 'User deleted successfully';
        redirect();
    }
    public function editAction()
    {
        if (!empty($_POST)) {
            $id = $this->getRequestID(false);
            $user = new \app\models\admin\User();
            $data = $_POST;
            $user->load($data);
            if (!$user->attributes['password']) {
                unset($user->attributes['password']);
            } else {
                $user->attributes['password'] = password_hash($user->attributes['password'], PASSWORD_DEFAULT);
            }
            if (!$user->validate($data) || !$user->checkUnique()) {
                $user->getErrors();
                redirect();
            }
            if ($user->update('user', $id)) {
                $_SESSION['success'] = 'Changes are saved';

            }
            redirect();
        }

        $user_id = $this->getRequestID();
        $user = \R::load('user', $user_id);
        $orders = \R::getAll("SELECT orders.id, orders.user_id, orders.date, p.tarif_name FROM orders join product p on orders.product_id = p.id
                                  WHERE user_id = {$user_id}   group by orders.id;");
        $this->setMeta('Editing a user profile');
        $this->set(compact('user', 'orders'));
    }
}