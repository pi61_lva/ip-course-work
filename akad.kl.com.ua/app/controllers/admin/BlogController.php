<?php


namespace app\controllers\admin;


use app\models\admin\Blog;

class BlogController extends AppController
{
    public function indexAction()
    {
        $articles = \R::getAll('SELECT blogpost.id, blogpost.title, blogpost.category_id, blogpost.short_text, blogpost.all_text, blogpost.publication_date, c.cat_name FROM blogpost
        join categories c on blogpost.category_id = c.id order by publication_date DESC');
        $this->set(compact('articles'));
        $this->setMeta('List of articles');
    }


    public function addAction()
    {
        if (!empty($_POST)) {
            $article = new Blog();
            $data = $_POST;
            $article->load($data);
            if (!$article->validate($data)) {
                $article->getErrors();
                $_SESSION['data'] = $data;
                redirect();
            }
            if ($id = $article->save('blogpost')) {
                $_SESSION['success'] = 'Add new article';
            }
            redirect();
        }

        $category = \R::findAll('categories');
        $this->set(compact('category'));
        $this->setMeta('New article');
    }

    public function editAction()
    {
        if (!empty($_POST)) {
            $id = $this->getRequestID(false);
            $blog = new Blog();
            $data = $_POST;

            $blog->load($data);
            if (!$blog->validate($data)) {
                $blog->getErrors();
                redirect();
            }
            if ($blog->update('blogpost', $id)) {
                $_SESSION['success'] = 'Changes are saved';
            }
            redirect();
        }

        $user_id = $this->getRequestID();
        $post = \R::load('blogpost', $user_id);
        $category = \R::find('categories');
        $this->setMeta('Editing a user profile');
        $this->set(compact('post', 'category'));

    }

    public function deleteAction()
    {
        $id = $this->getRequestID();
        $errors = '';

        if ($errors) {
            $_SESSION['error'] = $errors;
            redirect();
        }
        $category = \R::load('blogpost', $id);
        \R::trash($category);
        $_SESSION['success'] = 'Article deleted successfully';
        redirect();
    }
}