<?php


namespace app\controllers\admin;


use app\models\Category;
use app\models\AppModel;

class CategoryController extends AppController
{
    public function indexAction()
    {
        $category = \R::findAll('categories');
        $this->set(compact('category'));
        $this->setMeta('List of categories');
    }

    public function deleteAction()
    {
        $id = $this->getRequestID();
        $posts = \R::count('blogpost', 'id = ?', [$id]);
        $errors = '';
        if ($posts) {
            $errors .= 'Remove is not possible, there are articles in the category ';
        }
        if ($errors) {
            $_SESSION['error'] = $errors;
            redirect();
        }
        $category = \R::load('categories', $id);
        \R::trash($category);
        $_SESSION['success'] = 'Category deleted successfully';
        redirect();
    }

    public function addAction()
    {
        if (!empty($_POST)) {
            $category = new Category();
            $data = $_POST;
            $category->load($data);
            if ($category->validate($data)) {
                $category->getErrors();
                redirect();
            }
            if ($category->save('categories')) {
                $_SESSION['success'] = 'The category was added successfully';
            }
            redirect();
        }
        $this->setMeta('Adding a new category');
    }

    public function editAction()
    {
        if (!empty($_POST)) {
            $id = $this->getRequestID(false);
            $category = new Category();
            $data = $_POST;
            $category->load($data);
            if ($category->validate($data)) {
                $category->getErrors();
                redirect();
            }
            if ($category->update('categories', $id)) {
                $_SESSION['success'] = 'Update has been saved';
                redirect();
            }
        }
        $id = $this->getRequestID();
        $category = \R::load('categories', $id);
        $this->setMeta("Editing a {$category->cat_name} category");
        $this->set(compact('category'));
    }

}