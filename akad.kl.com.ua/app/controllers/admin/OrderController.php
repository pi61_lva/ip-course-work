<?php


namespace app\controllers\admin;


use app\models\Order;

class OrderController extends AppController
{
    public function indexAction()
    {
        $orders = \R::getAll('SELECT orders.id, orders.user_id, orders.date, user.name, user.login, p.tarif_name FROM orders join user ON orders.user_id = user.id
         join product p on orders.product_id = p.id group by orders.id ');

        $this->set(compact('orders'));
        $this->setMeta('List Suscribers');
    }
}