<?php


namespace app\controllers;


use app\models\User;

class UserController extends AppController
{
    public function signupAction()
    {
        if (!empty($_POST)) {
            $user = new User();
            $data = $_POST;
            $user->load($data);
            if (!$user->validate($data) || !$user->checkUnique()) {
                $user->getErrors();
                $_SESSION['form_data'] = $data;
            } else {
                $user->attributes['password'] = password_hash($user->attributes['password'], PASSWORD_DEFAULT);
                if ($user->save('user')) {
                    $_SESSION['success'] = 'You have successfully registered';
                    redirect(PATH . '/user/login');
                } else {
                    $_SESSION['error'] = 'ERROR !!!';
                }
            }
            redirect();
        }
        $this->setMeta('AKAD Sign Up');
    }

    public function loginAction()
    {
        if (!empty($_POST)) {
            $user = new User();
            if ($user->login()) {
                $_SESSION['success'] = 'You are successfully logged in';
                redirect(PATH);
            } else {
                $_SESSION['error'] = 'Check input value and try again!';
                redirect();
            }

        }

        $this->setMeta('AKAD Log In');
    }

    public function logoutAction()
    {
        if (isset($_SESSION['user'])) unset($_SESSION['user']);
        redirect();
    }

}