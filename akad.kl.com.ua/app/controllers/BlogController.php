<?php


namespace app\controllers;


class BlogController extends AppController
{

    //метод для виведення всіх постів
    public function blogviewAction()
    {
        $this->setMeta('AKAD Blog', 'AKAD Creative Agency', 'AKAD, Creative Agency');
        $posts = \R::findAll('blogpost', 'order by publication_date DESC');
        $postCategory = \R::findAll('categories');
        $this->set(compact('posts', 'postCategory'));
        if (!$posts) {
            throw new  \Exception('Сторінку не зайдено', 404);
        }
    }
}