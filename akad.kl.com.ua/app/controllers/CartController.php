<?php


namespace app\controllers;


use akad\App;
use app\models\Cart;
use app\models\Order;
use app\models\User;

class CartController extends AppController
{
    public function addAction()
    {
        $id = !empty($_GET['id']) ? (int)$_GET['id'] : null;
        if ($id) {
            $product = \R::findOne('product', 'id = ?', [$id]);
        }
        $cart = new Cart();
        $cart->addToCart($product);
        if ($this->isAjax()) {
            $this->loadView('cart_modal');
        }
        redirect();
    }

    public function deleteAction()
    {
        $id = !empty($_GET['id']) ? $_GET['id'] : null;
        if (isset($_SESSION['cart'][$id])) {
            $cart = new Cart();
            $cart->deleteItem($id);
        }
        if ($this->isAjax()) {
            $this->loadView('cart_modal');
        }
        redirect();
    }

    public function viewAction()
    {
        $this->setMeta('CART');
    }

    public function checkoutAction()
    {

        if (!empty($_POST)) {
            if (!User::checkAuth()) {
                $user = new User();
                $data = $_POST;
                $user->load($data);
                if (!$user->validate($data) || !$user->checkUnique()) {
                    $user->getErrors();
                    $_SESSION['form_data'] = $data;
                    redirect();
                } else {
                    $user->attributes['password'] = password_hash($user->attributes['password'], PASSWORD_DEFAULT);
                    if (!$user_id = $user->save('user')) {
                        $_SESSION['error'] = 'Error!';
                        redirect();
                    }
                }
            }

            if (!empty($_SESSION['user']['id'])) {
                $usr = \R::findAll('orders', 'where user_id = ?', [$_SESSION['user']['id']]);
            }

            if (empty($usr)) {
                $data['user_id'] = isset($user_id) ? $user_id : $_SESSION['user']['id'];
                $data['note'] = !empty($_POST['note']) ? $_POST['note'] : '';
                $user_email = isset($_SESSION['user']['email']) ? $_SESSION['user']['email'] : $_POST['email'];
                $order_id = Order::saveOrder($data);
                //для оплати (платіжної системи)
                if (!empty($_POST['pay'])) {
                    self::setPaymentData($order_id);
                }

                Order::mailOrder($order_id, $user_email);

                if (!empty($_POST['pay'])) {
                    redirect(PATH . '/payment/form.php');
                }
            } else {
                unset($_SESSION['cart']);
                $_SESSION['error'] = 'You already have an active tariff plan. You cannot have more active...';
            }
        }
        redirect();
    }

    protected static function setPaymentData($order_id)
    {
        var_dump($_SESSION);
        if (isset($_SESSION['payment'])) unset ($_SESSION['payment']);
        $_SESSION['payment']['id'] = $order_id;
        foreach ($_SESSION['cart'] as $price) {
            $_SESSION['payment']['price'] = $price['price'];
        }
        unset($_SESSION['cart']);
    }

    public function paymentAction()
    {
        if (empty($_POST)) {
            die;
        }
        $dataSet = $_POST;
        unset($dataSet['ik_sign']);
        ksort($dataSet, SORT_STRING);
        array_push($dataSet, App::$app->getProperty('ik_key'));
        $signString = implode(':', $dataSet);
        $sign = base64_encode(md5($signString, true));
        $id = (int)$dataSet['ik_pm_no'];
        $order = \R::getAll("SELECT user.id, product_id, p.price from user join orders o on user.id = o.user_id
         join product p on o.product_id = p.id where user.id = $id");
        if (!$order) die;
        if ($dataSet['ik_co_id'] != App::$app->getProperty('ik_id') || $dataSet['ik_inv_st'] != 'success' || $dataSet['ik_am'] != $order->price || $sign != $_POST['ik_sign']) {
            die;
        }
        \R::store($order);
        die;
    }
}