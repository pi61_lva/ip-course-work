<?php


namespace app\controllers;


class AboutController extends AppController
{
    public function aboutviewAction()
    {
        $this->setMeta('AKAD About Us', 'AKAD About Us', 'AKAD, Creative Agency');
        $teams = \R::findAll('aboutus_team');
        $info = \R::findAll('aboutus_info');
        $clients = \R::findAll('aboutus_ourclient');
        $clientFeed = \R::findAll('aboutus_clientfeedback');
        $this->set(compact('teams', 'clients', 'clientFeed', 'info'));
        if (!$teams) {
            throw new  \Exception('Сторінку не зайдено', 404);
        }
    }
}