<?php


namespace app\controllers;


class ServiceController extends AppController
{

    public function viewAction()
    {
        $this->setMeta('AKAD Service', 'AKAD Service', 'AKAD, Creative Agency');
        $products = \R::findAll('product');
        $this->set(compact('products'));
        if (!$products) {
            throw new  \Exception('Сторінку не зайдено', 404);
        }
    }
}