<?php


namespace app\controllers;


class PostController extends AppController
{
    public function postviewAction()
    {
        $this->setMeta('AKAD Blog', 'AKAD BLOGS', 'AKAD, Creative Agency');
        $id = $this->route['post'];
        $post = \R::findOne('blogpost', "id = ?", [$id]);
        $postCategory = \R::findAll('categories');
        $this->set(compact('post', 'postCategory'));
        if (!$post) {
            throw new  \Exception('Сторінку не зайдено', 404);
        }
    }
}