<?php


namespace app\controllers;


class SearchController extends AppController
{
    public function typeaheadAction()
    {
        if ($this->isAjax()) {
            $query = !empty(trim($_GET['query'])) ? trim($_GET['query']) : null;
            if ($query) {
                $posts = \R::getAll('SELECT id, title FROM blogpost WHERE title LIKE ? LIMIT 10', ["%{$query}%"]);
                echo json_encode($posts);
            }
        }
        die;
    }

    public function indexAction()
    {
        $query = !empty(trim($_GET['s'])) ? trim($_GET['s']) : null;
        if ($query) {
            $posts = \R::find('blogpost', 'title LIKE ? ORDER BY publication_date DESC', ["%{$query}%"]);
            $postCategory = \R::findAll('categories');
            $this->set(compact('posts', 'postCategory'));
        }

        $this->setMeta('SEARCH');
    }


}