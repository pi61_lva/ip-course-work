create database akad;

use akad;

CREATE TABLE user
(
    id       INT AUTO_INCREMENT NOT NULL PRIMARY KEY,
    login    VARCHAR(50)        NOT NULL,
    password VARCHAR(255)       NOT NULL,
    email    VARCHAR(255)       NOT NULL,
    name     VARCHAR(50)        NOT NULL,
    role     ENUM ('user','admin') DEFAULT 'user',
    constraint login unique (login),
    constraint email unique (email)
);

INSERT INTO user(login, password, email, name)
VALUES ('newUser', '12312', 'user1@ukr.net', 'Vlad');

CREATE TABLE product
(
    id          INT AUTO_INCREMENT NOT NULL PRIMARY KEY,
    tarif_name  VARCHAR(50)        NOT NULL,
    price       FLOAT              NOT NULL,
    privilegies VARCHAR(200)       NOT NULL,
    active_time time default '730:00:00'
);

INSERT INTO product(tarif_name, price, privilegies)
VALUES ('basic', 54.99, 'Team of 2-3 developers; Support after release 1 month; Free host for 3 months'),
       ('smart', 84.99,
        'Team of 7-10 developers; Support after release 3 month; Free host for a year; 24/7 technical support'),
       ('premium', 144.99,
        'Full team developers; Support after release 6 month; 24/7 technical support; Free host for a 3 years; VIP-service');
drop table blogpost;
CREATE TABLE blogpost
(
    id               INT AUTO_INCREMENT NOT NULL PRIMARY KEY,
    title            varchar(100)       not null,
    category_id      int references categories (id),
    publication_date TIMESTAMP          NOT NULL DEFAULT CURRENT_TIMESTAMP,
    short_text       varchar(500)       NOT NULL,
    all_text         varchar(10000)     NOT NULL,
    img_src          varchar(50)
);

INSERT INTO blogpost(publication_date, title, short_text, all_text, img_src, category_id)
VALUES ("2019-5-15", 'HTTP headers checker',
        'For any site, it is important to properly configure the HTTP headers. A lot of articles have been written on the subject of headings.',
        'For any site, it is important to properly configure the HTTP headers. A lot of articles have been written on the subject of headings. Here we have summarized the lessons learned, the RFC documentation. Some of the headings are mandatory, some are obsolete, some can be confusing and contradictory. We did a parsing to automatically check the HTTP headers of the web server.
Correct HTTP headers increase security and trust in the site, including from search engines, can affect the site’s position in Yandex and Google, save server resources, reduce server load, thereby increasing the server response speed, which again affects the ranking of the site in the search, save money on payment powerful hosting, which may not be required for the site when configured correctly.
Check not only web-page headers, but also headers of static content, images, css and js files. Check separately If-Modified-Since and If-None-Match for correct your web server reply 304 Not Modified.
', 'img_post1.jpg', 5),
       ("2019-5-30", 'All About Google App Engine (GAE): Features & Business Uses',
        'If I talk about Google then we all know that it provides an enormous range of tools, products, and services. In the running market, Google has scored high percentile and left a footprint in the list of world’s top 4 companies. Many creations of Google are widely used all over the world and the best example of this is Goole Search Engine.',
        'If I talk about Google then we all know that it provides an enormous range of tools, products, and services. In the running market, Google has scored high percentile and left a footprint in the list of world’s top 4 companies. Many creations of Google are widely used all over the world and the best example of this is Goole Search Engine. What Is Google App Engine?
        By the name, we can recognize that Google has created a Google App Engine, the name is similar to a search engine but its purpose is different. App Engine is a service and cloud computing platform employed for developing and hosting web applications.
        The platform supports Go, PHP, Java, Python, Node.js, .NET, and Ruby applications and apart from this it also supports other programming languages through custom runtimes. The App Engine serves 350 plus Billion requests per day.
    ', 'img_post2.jpg', 1),
       ("2019-4-2", 'Machine Learning & Big Data: Let’s Find The Relationship Between Them',
        'Machine learning is indeed a famous word among technologies. Today we will relate it with another famous term that is Big data. Both these have become Buzz words these days. Let’s here find out their meaning individually.',
        'Machine learning is indeed a famous word among technologies. Today we will relate it with another famous term that is Big data. Both these have become Buzz words these days. Let’s here find out their meaning individually.
        Big data is known as the process in which we collect and analyze the large volume of data sets (called Big Data) which helps in discovering useful hidden patterns and other information such as customer choices, market trends which is really beneficial for the organizations to remain informed and customer-oriented business decisions.
        Big data takes care of the extreme volume of data, different types of data types, and the velocity at which the data must be processed. Big data helps in taking better business decisions as well as the future steps that should be taken by the organizations.
        Machine learning is a subset of AI ( Artificial intelligence) which helps the computers and machines in predicting future actions without the intervention of human beings. So it could be said that, with the help of Machine learning, software applications can learn how to make their accuracy better in order to predict the outcomes.
        The most amazing thing is that Machine learning has become part of our day to day life as most people even don’t know that they are performing Machine learning activities in their daily life. It could be said that this is one of the most famous technologies that is being highly used by people these days.',
        'img_post3.jpg', 6),
       ("2020-06-03", 'UX Tips for Designing an Online Digital Product Store',
        'The future of business is digital. E-commerce business is gaining popularity with more and more people getting comfortable buying things online...', 'The future of business is digital. E-commerce business is gaining popularity with more and more people getting comfortable buying things online. While there are many factors that define the success of an E-commerce business, a major factor is E-Commerce UX, which refers to the overall experience of a user while shopping through an E-commerce platform. For the user experience to be enjoyable, it has to be personalized, relevant, and seamless. Hence there is a need to understand the target audience- their age, culture, level of tech literacy, channels they use, and their trust to online shopping. E-commerce UX should be data-driven, customer-driven, and user-centered. It should be built on research and validation. The main factors that influence the success of an E-commerce business are the quality of the product or service offered, how it is presented to the customer and how approachable is the design of the electronic platform through which sales are being done.
            A strong foundation needs well-built basics. That is why it is important to focus on the basic concepts of UX for a successful E-commerce platform. A simple and clear interaction is a major criteria. The user’s journey should be clear and easy, with no unnecessary clicks, time lost on loading pages, confusing steps, inconvenient menu, etc. Pleasant choice of colors, thoroughly thought-out logic and transitions, attractive and informative product presentation, data security, easily available contact info are some other factors that provide a desirable E-commerce UX. The goal should be to avoid cluttering and provide a seamless flow to the desired outcome.
        ', 'img_post4.jpg', 1);

#  # # # # # # # # # # # # # # # # # # #
SELECT title, category_id
FROM blog_post
         inner join categories c on blog_post.category_id = c.id;

SELECT *
from blog_post
         inner join categories c on blog_post.category_id = c.id;
#  # # # # # # # # # # # # # # # # # # #

CREATE TABLE blog_single
(
    id               INT AUTO_INCREMENT NOT NULL PRIMARY KEY,
    title            varchar(300)       NOT NULL, #заголовок посту
    publication_date date               NOT NULL,
    text             varchar(5000)      NOT NULL
);


CREATE TABLE categories
(
    id          INT AUTO_INCREMENT NOT NULL PRIMARY KEY,
    cat_name    varchar(100)       NOT NULL,
    keyword     varchar(100),
    description varchar(255)
);

INSERT INTO categories(cat_name)
VALUES ('Business'),
       ('Photography'),
       ('Design'),
       ('Scrience'),
       ('Server-development');


CREATE TABLE aboutus_team
(
    id       INT AUTO_INCREMENT NOT NULL PRIMARY KEY,
    img_src  varchar(100)       NOT NULL,
    name     varchar(80)        NOT NULL,
    position varchar(50)        NOT NULL
);

INSERT INTO aboutus_team(img_src, name, position)
VALUES ('dreamteam1.jpg', 'Jonh Doe', 'CEO / CO-FOUNDER/'),
       ('dreamteam2.jpg', 'JENNIFER TOMS', 'PROJECT CHIED'),
       ('dreamteam3.jpg', 'JACK DEC', 'GRAPHIC DESIGNER'),
       ('dreamteam4.jpg', 'ANNE TURIN', 'ART DIRECTOR');

CREATE TABLE aboutus_ourclient
(
    id      INT AUTO_INCREMENT NOT NULL PRIMARY KEY,
    logo    varchar(100)       NOT NULL,
    webpage varchar(70)        NOT NULL
);

INSERT INTO aboutus_ourclient(logo, webpage)
VALUES ('img1.jpg', 'https://steak-house.com.ua/'),
       ('img2.jpg', 'https://worldarchery.org/archery-equipment'),
       ('img3.jpg', 'https://atelierdelarmee.com/'),
       ('img4.jpg', 'https://www.behance.net/gallery/13705129/Pacificana-Co'),
       ('img5.jpg', 'https://www.theknot.com/'),
       ('img6.jpg', 'http://www.themerchantatl.com/');

SELECT *
FROM aboutus_ourclient;

CREATE TABLE aboutus_clientFeedback
(
    id          INT AUTO_INCREMENT NOT NULL PRIMARY KEY,
    client_img  varchar(100)       NOT NULL,
    client_name varchar(80)        NOT NULL,
    text        varchar(400)       NOT NULL
);

INSERT INTO aboutus_clientFeedback(client_img, client_name, text)
VALUES ('Layer 13', 'The Merchant',
        'Quisque iaculis lorem vestibulum eros vehicula, non congue elit dictum. Donec mollis aliquet lorem, eu porttitor sapien semper in. Duis ac erat luctus, gravida lectus sit amet, consectetur orci. Suspendisse libero mauris.');

CREATE TABLE aboutus_info
(
    id            INT AUTO_INCREMENT NOT NULL PRIMARY KEY,
    section_title varchar(100)       NOT NULL,
    small_title   varchar(100)       NOT NULL,
    text          varchar(1000)      NOT NULL
);

INSERT INTO aboutus_info(section_title, small_title, text)
VALUES ('ABOUT US', 'WE ARE AWESOME', 'AKAD is a California-based digital agency founded in 2010. Their team works with enterprise clients on their UX design systems, design, development, and branding needs.
AKAD developed a website focused on employee benefits for a Fortune 500 media group. Isadora began their work by assessing the company''s existing site and researching how other companies designed sites geared toward employee benefits. They created a new look for the site creating all designs from scratch and integrating with WordPress, which ultimately helped form a new strategic marketing tool.');

INSERT INTO aboutus_info(section_title, small_title, text)
VALUES ('WHAT WE DO', 'CREATIVE & DIGITAL', 'Whether it’s a pop-up meal, a visual study of a sector, or a training kit, all Creative Agency projects are influenced by systems and people-oriented thinking, and end in targeted insights, programs or products that move forward your organizational goals.
All Creative Agency projects start with a discovery phase in which we understand the core goals of your effort and apply the Creative Agency approach to understanding the larger opportunities you might have. Creative Agency projects can be categorized into three major types that are linked by a continuum of transforming information into action and experience.
');

CREATE TABLE orders
(
    id         INT AUTO_INCREMENT NOT NULL PRIMARY KEY,
    user_id    int references user (id),
    date       timestamp          NOT NULL DEFAULT CURRENT_TIMESTAMP,
    note       varchar(255),
    product_id int references product (id)
);

# виведення користувачі з активним тарифним планом(підпискою)
SELECT orders.id, orders.user_id, orders.date, user.name, p.tarif_name
FROM orders
         join user ON orders.user_id = user.id
         join product p on orders.product_id = p.id
group by orders.id;

# виведення користувача, його активного тарифного плану за полем user_id
SELECT orders.id, orders.user_id, orders.date, p.tarif_name
FROM orders
         join product p on orders.product_id = p.id
WHERE user_id = {$user_id}
group by orders.id;

SELECT blog_post.id, blog_post.title, blog_post.category_id, blog_post.short_text, blog_post.all_text, c.cat_name
FROM blog_post
         join categories c on blog_post.category_id = c.id



SELECT user.login, o.id, p.tarif_name
from user
         join orders o on user.id = o.user_id
         join product p on o.product_id = p.id
where user_id = 6