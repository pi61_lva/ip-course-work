<?php session_start();
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<p>After a few seconds, you will be redirected to the payment page. click the button if you don't want to wait</p>
<?php if (!empty($_SESSION['payment'])): ?>
    <form id="payment" name="payment" method="post" action="https://sci.interkassa.com/" enctype="utf-8">
        <input type="hidden" name="ik_co_id" value="5ed7cd521ae1bd0e028b4569"/>
        <input type="hidden" name="ik_pm_no" value="<?= $_SESSION['payment']['id']; ?>"/>
        <input type="hidden" name="ik_am" value="<?= $_SESSION['payment']['price']; ?>"/>
        <input type="hidden" name="ik_cur" value="UAH"/>
        <input type="hidden" name="ik_desc" value="Paying for a subscription in AKAD Agency"/>
        <input type="submit" value="Pay">
    </form>
<? endif; ?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
<script>
    setTimeout(function () {
        $('form').submit();
    }, 2000);
</script>

</body>
</html>