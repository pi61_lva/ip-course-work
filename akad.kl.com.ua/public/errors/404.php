<!DOCTYPE html>
<html>
<head>
    <title>AKAD Error 404</title>
    <link rel="stylesheet" href="/public/errors/css/style.css">
    <link
      href='//fonts.googleapis.com/css?family=Source+Sans+Pro:400,200italic,200,300,300italic,400italic,600,600italic,700,700italic,900,900italic'
      rel='stylesheet' type='text/css'>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="keywords"
          content="Simple Error Page Widget Responsive, Login Form Web Template, Flat Pricing Tables, Flat Drop-Downs, Sign-Up Web Templates, Flat Web Templates, Login Sign-up Responsive Web Template, Smartphone Compatible Web Template, Free Web Designs for Nokia, Samsung, LG, Sony Ericsson, Motorola Web Design"/>
    <script type="application/x-javascript"> addEventListener("load", function () {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        } </script>
</head>
<body>
<div class="main w3l">
    <h2>OOPS</h2>
    <h1> ERROR 404</h1>
    <h3>"Sorry! The page you are looking for can't be found"</h3>
    <a href="<?= PATH; ?>" class="back">BACK TO HOME</a>
    <div class="social-icons w3">
        <ul>
            <li><a class="twitter" href="#"></a></li>
            <li><a class="facebook" href="#"></a></li>
            <li><a class="pinterest" href="#"></a></li>
        </ul>
    </div>
</div>

</body>
</html>