<?php

use akad\Router;

Router::add('^blog/(?P<post>[a-z0-9-]+)/?$', ['controller' => 'Post', 'action' => 'postview']);
Router::add('^blog/(?P<search>[a-z0-9-]+)/?$', ['controller' => 'Search', 'action' => 'index']);
Router::add('^category/(?P<post>[a-z0-9-]+)/?$', ['controller' => 'Category', 'action' => 'view']);

Router::add('^blog$', ['controller' => 'Blog', 'action' => 'blogview']);
Router::add('^about$', ['controller' => 'About', 'action' => 'aboutview']);
Router::add('^service$', ['controller' => 'Service', 'action' => 'view']);


//default routes
Router::add('^admin$', ['controller' => 'Main', 'action' => 'index', 'prefix' => 'admin']);
Router::add('^admin/?(?P<controller>[a-z-]+)/?(?P<action>[a-z-]+)?$', ['prefix' => 'admin']);

Router::add('^$', ['controller' => 'Main', 'action' => 'index']);
Router::add('^(?P<controller>[a-z-]+)/?(?P<action>[a-z-]+)?$');

