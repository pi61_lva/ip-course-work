<?php
define("DEBUG", 0);  // 1 режим розробки, 0 - прод.
define("ROOT", dirname(__DIR__));
define("WWW", ROOT . '/public');
define("APP", ROOT . '/app');
define("CORE", ROOT . '/vendor/akad/core');
define("LIBS", ROOT . '/vendor/akad/core/libs');
define("CACHE", ROOT . '/tmp/cache');
define("CONF", ROOT . '/config');
define("LAYOUT", 'AKAD'); //для деф. шаблонку


$app_path = "http://{$_SERVER["HTTP_HOST"]}{$_SERVER['PHP_SELF']}";
$app_path = preg_replace("#[^/]+$#", '', $app_path);
$app_path = str_replace('/public/', '', $app_path);


define("PATH", $app_path);
define("ADMIN", PATH . '/admin');

require_once ROOT . '/vendor/autoload.php';